package ac.uk.bolton.hospitalrun.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:09
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Patient {
   private int patientId;
   private int patientTypeCode;
   private String prefix;
   private String givenName;
   private String familyName;
   private String suffix;
   private String gender;
   private String dateOfBirth;
   private String language;
   private String occupation;
   private String phonenNumber;
   private String emailAddress;
   private String address;
}
