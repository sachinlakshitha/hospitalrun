package ac.uk.bolton.hospitalrun.model;

import java.io.Serializable;

public class JwtResponse implements Serializable {
    private String id;
    private String username;
    private String role;
    private String token;
    
    public JwtResponse(String id, String username, String role, String jwttoken) {
        this.id = id;
        this.username = username;
        this.role = role;
        this.token = jwttoken;
    }
   
    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
   
    public String getToken() {
        return token;
    }

    public void setToken(String jwttoken) {
        this.token = jwttoken;
    }

    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
       
    
}
