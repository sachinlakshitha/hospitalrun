package ac.uk.bolton.hospitalrun.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import javax.persistence.*;

import java.util.Date;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:53
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class Appointment {
    private Integer appointmentId;
    private Integer patientId;
    @Temporal(TemporalType.TIMESTAMP)
    private Date startDate;
    @Temporal(TemporalType.TIMESTAMP)
    private Date endDate;
    private String location;
    private String appointmentType;
    private String reason;
}
