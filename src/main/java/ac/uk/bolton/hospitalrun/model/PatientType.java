package ac.uk.bolton.hospitalrun.model;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:09
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatientType {
    private int patientTypeCode;
    private String patientTypeDesc;
}
