/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.RoleDao;
import ac.uk.bolton.hospitalrun.dao.UserDao;
import ac.uk.bolton.hospitalrun.model.UserModel;
import ac.uk.bolton.hospitalrun.model.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

/**
 *
 * @author Sachin
 */
@Service
public class UserDetailsServiceImpl implements UserDetailsService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        User user = userDao.findByUsername(username);

        if (user == null) {
            throw new UsernameNotFoundException("User not found with username: " + username);
        }

        UserModel userModel = new UserModel();

        userModel.setUserId(user.getUserId());
        userModel.setUsername(user.getUsername());
        userModel.setPassword(user.getPassword());
        
        try {
            userModel.setRole(roleDao.findById(user.getRoleId()).getRoleName());
        } catch (Exception e) {

        }

        return UserDetailsImpl.build(userModel);
    }

}
