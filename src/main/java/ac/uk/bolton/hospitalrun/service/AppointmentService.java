package ac.uk.bolton.hospitalrun.service;

import ac.uk.bolton.hospitalrun.dto.AppoinmentDTO;

import java.util.List;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:58
 */
public interface AppointmentService {
    public AppoinmentDTO save(AppoinmentDTO appoinment) throws Exception;
    public AppoinmentDTO findById(Integer id) throws Exception;
    public List<AppoinmentDTO> readAll() throws Exception;
    public boolean update(Integer id,AppoinmentDTO appoinment) throws Exception;
    public boolean deleteById(Integer id) throws Exception;
    public List<AppoinmentDTO> readAppointmentsByPatientsId(Integer patientId) throws Exception;
    public List<AppoinmentDTO> getCurrentDateAppointment() throws Exception;
}
