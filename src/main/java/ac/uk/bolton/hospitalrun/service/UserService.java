/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.service;

import ac.uk.bolton.hospitalrun.dto.UserDto;
import ac.uk.bolton.hospitalrun.dto.UserPasswordDto;
import ac.uk.bolton.hospitalrun.model.User;
import java.util.List;
import java.util.Map;

/**
 *
 * @author Sachin
 */
public interface UserService {
    public User save(User user) throws Exception;
    public List<UserDto> readAll() throws Exception;
    public UserDto findById(String id) throws Exception;
    public boolean deleteById(String id) throws Exception;
    public Map update(UserPasswordDto user) throws Exception;
}
