package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.AppointmentDao;
import ac.uk.bolton.hospitalrun.dao.PatientDao;
import ac.uk.bolton.hospitalrun.dto.AppoinmentDTO;
import ac.uk.bolton.hospitalrun.dto.PatientDTO;
import ac.uk.bolton.hospitalrun.model.Appointment;
import ac.uk.bolton.hospitalrun.model.Patient;
import ac.uk.bolton.hospitalrun.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.mail.javamail.JavaMailSender;
import org.springframework.mail.javamail.MimeMessageHelper;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import javax.mail.MessagingException;
import javax.mail.internet.MimeMessage;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:59
 */
@Transactional
@Service
public class AppointmentServiceImpl implements AppointmentService {

    @Autowired
    private AppointmentDao appointmentDao;
    @Autowired
    private PatientDao patientDao;
    @Autowired
    private JavaMailSender javaMailSender;

    final String DATE_FORMAT = "yyyy-MM-dd HH:mm:ss";

    @Override
    public AppoinmentDTO save(AppoinmentDTO appoinment) throws Exception {
        Appointment a = new Appointment();

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT, Locale.ENGLISH);
        a.setAppointmentId(appoinment.getAppointmentId());
        a.setPatientId(appoinment.getPatientId());
        a.setStartDate(sdf.parse(appoinment.getStartDate()));
        a.setEndDate(sdf.parse(appoinment.getEndDate()));
        a.setLocation(appoinment.getLocation());
        a.setAppointmentType(appoinment.getAppointmentType());
        a.setReason(appoinment.getReason());
        int save = appointmentDao.save(a);
        sendMailToPatient(patientDao.findById(appoinment.getPatientId()),appoinment);
        return save > 0 ? appoinment : null;
    }

    @Override
    public AppoinmentDTO findById(Integer id) throws Exception {
        if (id == null) {
            return null;
        }
        AppoinmentDTO appointment = getAppointment(appointmentDao.findById(id));
        Patient patient = patientDao.findById(appointment.getPatientId());
        appointment.setPatient(getPatient(patient));
        return appointment;
    }

    @Override
    public List<AppoinmentDTO> readAll() throws Exception {
        List<AppoinmentDTO> appointmentList = new ArrayList<>();

        for (Appointment appointment : appointmentDao.readAll()) {
            AppoinmentDTO app = getAppointment(appointment);
            Patient patient = patientDao.findById(appointment.getPatientId());
            app.setPatient(getPatient(patient));
            appointmentList.add(app);
        }
        return appointmentList;
    }

    @Override
    public boolean update(Integer id, AppoinmentDTO appoinment) throws Exception {
        Appointment app = appointmentDao.findById(id);
        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        app.setPatientId(appoinment.getPatientId());
        app.setStartDate(sdf.parse(appoinment.getStartDate()));
        app.setEndDate(sdf.parse(appoinment.getEndDate()));
        app.setLocation(appoinment.getLocation());
        app.setAppointmentType(appoinment.getAppointmentType());
        app.setReason(appoinment.getReason());

        return appointmentDao.update(app) > 0 ? true : false;
    }

    @Override
    public boolean deleteById(Integer id) throws Exception {
       if (id == null) {
           return false;
       }
        return appointmentDao.deleteById(id) > 0 ? true : false;
    }

    @Override
    public List<AppoinmentDTO> readAppointmentsByPatientsId(Integer patientId) throws Exception {
        if (patientId == null) {
            return null;
        }
        List<Appointment> appointmentList = appointmentDao.readAppointmentsByPatientsId(patientId);
        if (appointmentList == null || appointmentList.isEmpty()) {
            return null;
        }
        List<AppoinmentDTO> appoinmentDTOList = new ArrayList<>();
        for (Appointment appointment : appointmentList) {
            AppoinmentDTO app = getAppointment(appointment);
            Patient patient = patientDao.findById(app.getPatientId());
            app.setPatient(getPatient(patient));
            appoinmentDTOList.add(app);
        }
        return appoinmentDTOList;
    }

    @Override
    public List<AppoinmentDTO> getCurrentDateAppointment() throws Exception {
        List<Appointment> currentDateAppointment = appointmentDao.getCurrentDateAppointment();
        if (currentDateAppointment == null || currentDateAppointment.isEmpty()) {
            return null;
        }
        List<AppoinmentDTO> appoinmentDTOList = new ArrayList<>();
        for (Appointment appointment : currentDateAppointment) {
            AppoinmentDTO app = getAppointment(appointment);
            Patient patient = patientDao.findById(app.getPatientId());
            app.setPatient(getPatient(patient));
            appoinmentDTOList.add(app);
        }
        return appoinmentDTOList;
    }

    public AppoinmentDTO getAppointment(Appointment appointment){
        AppoinmentDTO a = new AppoinmentDTO();

        SimpleDateFormat sdf = new SimpleDateFormat(DATE_FORMAT);
        a.setAppointmentId(appointment.getAppointmentId());
        a.setPatientId(appointment.getPatientId());
        a.setStartDate(sdf.format(appointment.getStartDate()));
        a.setEndDate(sdf.format(appointment.getEndDate()));
        a.setLocation(appointment.getLocation());
        a.setAppointmentType(appointment.getAppointmentType());
        a.setReason(appointment.getReason());

        return a;
    }

    public PatientDTO getPatient(Patient patient){
        PatientDTO p = new PatientDTO();

        p.setPatientId(patient.getPatientId());
        p.setPatientTypeCode(patient.getPatientTypeCode());
        p.setPrefix(patient.getPrefix());
        p.setGivenName(patient.getGivenName());
        p.setFamilyName(patient.getFamilyName());
        p.setSuffix(patient.getSuffix());
        p.setGender(patient.getGender());
        p.setDateOfBirth(patient.getDateOfBirth());
        p.setLanguage(patient.getLanguage());
        p.setOccupation(patient.getOccupation());
        p.setPhonenNumber(patient.getPhonenNumber());
        p.setEmailAddress(patient.getEmailAddress());
        p.setAddress(patient.getAddress());
        return p;
    }

    private void sendMailToPatient(Patient patient, AppoinmentDTO appointment) throws MessagingException {
        MimeMessage mimeMessage = javaMailSender.createMimeMessage();
        MimeMessageHelper mimeMessageHelper = new MimeMessageHelper(mimeMessage,true);
        mimeMessageHelper.setFrom("baylorhealthcare24@gmail.com");
        mimeMessageHelper.setTo(patient.getEmailAddress());
        mimeMessageHelper.setSubject(patient.getGivenName()+"'S" +" "+ "Appointment Details");
        mimeMessageHelper.setText("Patient Name : "+patient.getGivenName()+"\n" + "Appointment Date : "+appointment.getStartDate() +"\n");

        javaMailSender.send(mimeMessage);

    }
}
