package ac.uk.bolton.hospitalrun.service;

import ac.uk.bolton.hospitalrun.dto.PatientTypeDTO;

import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:30
 */
public interface PatientTypeService {
    public PatientTypeDTO save(PatientTypeDTO p) throws Exception;
    public PatientTypeDTO findById(Integer id) throws Exception;
    public List<PatientTypeDTO> readAll() throws Exception;
    public boolean update(Integer id,PatientTypeDTO p) throws Exception;
    public boolean deleteById(Integer id) throws Exception;
}
