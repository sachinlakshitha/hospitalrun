package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.PatientTypeDao;
import ac.uk.bolton.hospitalrun.dto.PatientTypeDTO;
import ac.uk.bolton.hospitalrun.model.PatientType;
import ac.uk.bolton.hospitalrun.service.PatientTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.ArrayList;
import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:31
 */
@Transactional
@Service
public class PatientTypeServiceImpl implements PatientTypeService {

    @Autowired
    private PatientTypeDao patientTypeDao;

    @Override
    public PatientTypeDTO save(PatientTypeDTO p) throws Exception {
        PatientType type = new PatientType();

        type.setPatientTypeDesc(p.getPatientTypeDesc());

        return patientTypeDao.save(type) > 0 ? p : null;
    }

    @Override
    public PatientTypeDTO findById(Integer id) throws Exception {
        if (id == null) {
            return null;
        }
        return getPatientType(patientTypeDao.findById(id));
    }

    @Override
    public List<PatientTypeDTO> readAll() throws Exception {
        List<PatientTypeDTO> typeList = new ArrayList<>();

        for (PatientType type : patientTypeDao.readAll()) {
            typeList.add(getPatientType(type));
        }
        return typeList;
    }

    @Override
    public boolean update(Integer id,PatientTypeDTO p) throws Exception {
        PatientType type = patientTypeDao.findById(id);

        type.setPatientTypeDesc(p.getPatientTypeDesc());

        return patientTypeDao.update(type) > 0 ? true : false;
    }

    @Override
    public boolean deleteById(Integer id) throws Exception {
        if (id == null) {
            return false;
        }
        return patientTypeDao.deleteById(id) > 0 ? true : false;
    }

    public PatientTypeDTO getPatientType(PatientType patientType){
        PatientTypeDTO type = new PatientTypeDTO();

        type.setPatientTypeCode(patientType.getPatientTypeCode());
        type.setPatientTypeDesc(patientType.getPatientTypeDesc());

        return type;
    }
}
