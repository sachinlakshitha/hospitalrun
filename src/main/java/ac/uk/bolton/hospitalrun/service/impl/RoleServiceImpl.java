/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.RoleDao;
import ac.uk.bolton.hospitalrun.dto.RoleDto;
import ac.uk.bolton.hospitalrun.model.Role;
import ac.uk.bolton.hospitalrun.service.RoleService;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class RoleServiceImpl implements RoleService{
    @Autowired
    private RoleDao roleDao;
    
    @Override
    public RoleDto save(RoleDto roleDto) throws Exception {
        Role role = new Role();
        
        role.setRoleId(UUID.randomUUID().toString());
        role.setRoleName(roleDto.getRoleName());
        
        return roleDao.save(role) > 0 ? roleDto : null;
    }

    @Override
    public RoleDto findById(String id) throws Exception {
        Role role = roleDao.findById(id);
        
        if(role != null){
            return getRoleDto(role);
        }else{
            return null;
        }    
    }

    @Override
    public List<RoleDto> readAll() throws Exception {
        List<RoleDto> roleDtoList = new ArrayList<>();
        
        for (Role role : roleDao.readAll()) {
            roleDtoList.add(getRoleDto(role));
        }
        
        return roleDtoList;
    }

    @Override
    public boolean update(RoleDto roleDto) throws Exception {
        Role role = new Role();
        
        role.setRoleId(roleDto.getRoleId());
        role.setRoleName(roleDto.getRoleName());
        
        return roleDao.update(role) > 0 ? true : false;
    }

    @Override
    public boolean deleteById(String id) throws Exception {
        return roleDao.deleteById(id) > 0 ? true : false;
    }
    
    public RoleDto getRoleDto(Role role){
        RoleDto roleDto = new RoleDto();
        
        roleDto.setRoleId(role.getRoleId());
        roleDto.setRoleName(role.getRoleName());
        
        return roleDto;
    }
}
