/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.service;

import ac.uk.bolton.hospitalrun.dto.RoleDto;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface RoleService {
    public RoleDto save(RoleDto roleDto) throws Exception;
    public RoleDto findById(String id) throws Exception;
    public List<RoleDto> readAll() throws Exception;
    public boolean update(RoleDto roleDto) throws Exception;
    public boolean deleteById(String id) throws Exception;
}
