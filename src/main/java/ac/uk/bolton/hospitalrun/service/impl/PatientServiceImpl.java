package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.PatientDao;
import ac.uk.bolton.hospitalrun.dto.PatientDTO;
import ac.uk.bolton.hospitalrun.model.Patient;
import ac.uk.bolton.hospitalrun.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.logging.Logger;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:12
 */
@Transactional
@Service
public class PatientServiceImpl implements PatientService {

    @Autowired
    private PatientDao patientDao;

    @Override
    public PatientDTO save(PatientDTO patient) throws Exception {
        Patient p = new Patient();

        p.setPatientId(patient.getPatientId());
        p.setPatientTypeCode(patient.getPatientTypeCode());
        p.setPrefix(patient.getPrefix());
        p.setGivenName(patient.getGivenName());
        p.setFamilyName(patient.getFamilyName());
        p.setSuffix(patient.getSuffix());
        p.setGender(patient.getGender());
        p.setDateOfBirth(patient.getDateOfBirth());
        p.setLanguage(patient.getLanguage());
        p.setOccupation(patient.getOccupation());
        p.setPhonenNumber(patient.getPhonenNumber());
        p.setEmailAddress(patient.getEmailAddress());
        p.setAddress(patient.getAddress());

        return patientDao.save(p) > 0 ? patient : null;
    }

    @Override
    public PatientDTO findById(Integer id) throws Exception {
        if (id == null) {
            return null;
        }
        return getPatient(patientDao.findById(id));
    }

    @Override
    public List<PatientDTO> readAll() throws Exception {
        List<PatientDTO> patientDTOList = new ArrayList<>();

        for (Patient patient : patientDao.readAll()) {
            patientDTOList.add(getPatient(patient));
        }
        return patientDTOList;
    }

    @Override
    public boolean update(Integer id,PatientDTO patient) throws Exception {
        Patient p = patientDao.findById(id);

        p.setPatientTypeCode(patient.getPatientTypeCode());
        p.setPrefix(patient.getPrefix());
        p.setGivenName(patient.getGivenName());
        p.setFamilyName(patient.getFamilyName());
        p.setSuffix(patient.getSuffix());
        p.setGender(patient.getGender());
        p.setDateOfBirth(patient.getDateOfBirth());
        p.setLanguage(patient.getLanguage());
        p.setOccupation(patient.getOccupation());
        p.setPhonenNumber(patient.getPhonenNumber());
        p.setEmailAddress(patient.getEmailAddress());
        p.setAddress(patient.getAddress());

        return patientDao.update(p) > 0 ? true : false;
    }

    @Override
    public boolean deleteById(Integer id) throws Exception {
        if (id == null) {
            return false;
        }
        return patientDao.deleteById(id) > 0 ? true : false;
    }

    public PatientDTO getPatient(Patient patient){
        PatientDTO p = new PatientDTO();

        p.setPatientId(patient.getPatientId());
        p.setPatientTypeCode(patient.getPatientTypeCode());
        p.setPrefix(patient.getPrefix());
        p.setGivenName(patient.getGivenName());
        p.setFamilyName(patient.getFamilyName());
        p.setSuffix(patient.getSuffix());
        p.setGender(patient.getGender());
        p.setDateOfBirth(patient.getDateOfBirth());
        p.setLanguage(patient.getLanguage());
        p.setOccupation(patient.getOccupation());
        p.setPhonenNumber(patient.getPhonenNumber());
        p.setEmailAddress(patient.getEmailAddress());
        p.setAddress(patient.getAddress());
        return p;
    }
}
