/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.service.impl;

import ac.uk.bolton.hospitalrun.dao.RoleDao;
import ac.uk.bolton.hospitalrun.dao.UserDao;
import ac.uk.bolton.hospitalrun.dto.UserDto;
import ac.uk.bolton.hospitalrun.dto.UserPasswordDto;
import ac.uk.bolton.hospitalrun.model.User;
import ac.uk.bolton.hospitalrun.service.UserService;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author Sachin
 */
@Transactional
@Service
public class UserServiceImpl implements UserService {

    @Autowired
    private UserDao userDao;
    @Autowired
    private RoleDao roleDao;
    @Autowired
    private PasswordEncoder bcryptEncoder;

    @Override
    public User save(User user) throws Exception {
        user.setUserId(UUID.randomUUID().toString());
        user.setPassword(bcryptEncoder.encode(user.getPassword()));

        int result = userDao.save(user);
        return result > 0 ? user : null;
    }

    @Override
    public List<UserDto> readAll() throws Exception {
        List<UserDto> userDtoList = new ArrayList<>();

        for (User user : userDao.readAll()) {
            userDtoList.add(getUserDto(user));
        }

        return userDtoList;
    }

    @Override
    public UserDto findById(String id) throws Exception {
        User user = userDao.findById(id);

        if (user != null) {
            return getUserDto(user);
        } else {
            return null;
        }
    }

    @Override
    public boolean deleteById(String id) throws Exception {
        return userDao.deleteById(id) > 0 ? true : false;
    }

    @Override
    public Map update(UserPasswordDto userDto) throws Exception {
        String password = userDto.getPassword();

        if (password == null) {
            User newUser = new User();

            newUser.setUserId(userDto.getUserId());
            newUser.setUsername(userDto.getUsername());
            newUser.setPassword(null);
            newUser.setRoleId(userDto.getRoleId());

            int isUpdated = userDao.update(newUser);

            if (isUpdated > 0) {
                Map map = new HashMap<>();
                map.put("status", "200");
                map.put("msg", "User updated successfully");

                return map;
            } else {
                Map map = new HashMap<>();
                map.put("status", "304");
                map.put("msg", "User update failed");

                return map;
            }
            
        } else {
            String userId = userDto.getUserId();

            User user = userDao.findById(userId);

            boolean isMatched = bcryptEncoder.matches(userDto.getPassword(), user.getPassword());

            if (isMatched) {
                User newUser = new User();

                newUser.setUserId(userDto.getUserId());
                newUser.setUsername(userDto.getUsername());
                newUser.setPassword(bcryptEncoder.encode(userDto.getNewpassword()));
                newUser.setRoleId(userDto.getRoleId());

                int isUpdated = userDao.update(newUser);

                if (isUpdated > 0) {
                    Map map = new HashMap<>();
                    map.put("status", "200");
                    map.put("msg", "User updated successfully");

                    return map;
                } else {
                    Map map = new HashMap<>();
                    map.put("status", "304");
                    map.put("msg", "User update failed");

                    return map;
                }

            } else {
                Map map = new HashMap<>();
                map.put("status", "204");
                map.put("msg", "Current password is wrong!");

                return map;
            }
        }
    }

    public UserDto getUserDto(User user) throws Exception {
        UserDto userDto = new UserDto();

        userDto.setUserId(user.getUserId());
        userDto.setUsername(user.getUsername());
        userDto.setRole(roleDao.findById(user.getRoleId()).getRoleName());

        return userDto;
    }
}
