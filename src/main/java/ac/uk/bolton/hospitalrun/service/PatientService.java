package ac.uk.bolton.hospitalrun.service;

import ac.uk.bolton.hospitalrun.dto.PatientDTO;

import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:11
 */
public interface PatientService {
    public PatientDTO save(PatientDTO p) throws Exception;
    public PatientDTO findById(Integer id) throws Exception;
    public List<PatientDTO> readAll() throws Exception;
    public boolean update(Integer id,PatientDTO p) throws Exception;
    public boolean deleteById(Integer id) throws Exception;
}
