/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.controller;

import ac.uk.bolton.hospitalrun.dto.UserDto;
import ac.uk.bolton.hospitalrun.dto.UserPasswordDto;
import ac.uk.bolton.hospitalrun.model.User;
import ac.uk.bolton.hospitalrun.service.UserService;
import java.util.Map;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sachin
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class UserController {
    @Autowired
    private UserService userService;
        
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping(value = "/users")
    public ResponseEntity addUser(@RequestBody User user) throws Exception {
        User userObj = userService.save(user);
        
        if(userObj == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }  
    }
            
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping(value="/users/{id}")
    public ResponseEntity findUser(@PathVariable String id) throws Exception{
        UserDto user = userService.findById(id);
        
        if(user == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(user);
        }     
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping(value="/users")
    public ResponseEntity listUser() throws Exception{
        return ResponseEntity.ok(userService.readAll());
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @DeleteMapping(value="/users/{id}")
    public ResponseEntity deleteUser(@PathVariable String id) throws Exception{
        boolean isDeleted = userService.deleteById(id);
        
        if(isDeleted){
            return ResponseEntity.ok("");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }       
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PutMapping(value="/users")
    public ResponseEntity<String> updateUser(@RequestBody UserPasswordDto user) throws Exception{
        Map responseMap = userService.update(user);
        
        String status = responseMap.get("status").toString();
        
        if(status.equalsIgnoreCase("200")){
           return ResponseEntity.ok(responseMap.get("msg").toString());
        }else if(status.equalsIgnoreCase("204")){
            return new ResponseEntity<String>(responseMap.get("msg").toString(),HttpStatus.NO_CONTENT);
        }else{
            return new ResponseEntity<String>(responseMap.get("msg").toString(),HttpStatus.NOT_MODIFIED);
        }     
    }
}
