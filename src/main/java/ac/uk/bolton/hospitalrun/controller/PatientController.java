package ac.uk.bolton.hospitalrun.controller;

import ac.uk.bolton.hospitalrun.dto.PatientDTO;
import ac.uk.bolton.hospitalrun.service.PatientService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:08
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class PatientController {

    @Autowired
    private PatientService patientService;

    @PostMapping(value = "/patient/add")
    public ResponseEntity addPatient(@RequestBody PatientDTO dto) throws Exception {
        PatientDTO patient = patientService.save(dto);

        if(patient == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @GetMapping(value="/patient/{id}")
    public ResponseEntity getPatient(@PathVariable Integer id) throws Exception{
        PatientDTO patient = patientService.findById(id);

        if(patient == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(patient);
        }
    }

    @GetMapping(value="/patients")
    public ResponseEntity getPatientList() throws Exception{
        return ResponseEntity.ok(patientService.readAll());
    }

    @DeleteMapping(value="/patient/{id}")
    public ResponseEntity removePatient(@PathVariable Integer id) throws Exception{
        boolean isRemoved = patientService.deleteById(id);

        if(isRemoved){
            return ResponseEntity.ok("Patient Removed Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value="/patient/{id}")
    public ResponseEntity updatePatient(@PathVariable Integer id,@RequestBody PatientDTO dto) throws Exception{
        boolean isUpdated = patientService.update(id,dto);

        if(isUpdated){
            return ResponseEntity.ok("Patient Details Modified Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
