/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.controller;

import ac.uk.bolton.hospitalrun.dto.RoleDto;
import ac.uk.bolton.hospitalrun.service.RoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author Sachin
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class RoleController {
   @Autowired
   private RoleService roleService;
   
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PostMapping(value = "/roles")
    public ResponseEntity addRole(@RequestBody RoleDto roleDto) throws Exception {
        RoleDto roleObj = roleService.save(roleDto);
        
        if(roleObj == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }  
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping(value="/roles/{id}")
    public ResponseEntity findRole(@PathVariable String id) throws Exception{
        RoleDto role = roleService.findById(id);
        
        if(role == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(role);
        }     
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @GetMapping(value="/roles")
    public ResponseEntity listRole() throws Exception{
        return ResponseEntity.ok(roleService.readAll());
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @DeleteMapping(value="/roles/{id}")
    public ResponseEntity deleteRole(@PathVariable String id) throws Exception{
        boolean isDeleted = roleService.deleteById(id);
        
        if(isDeleted){
            return ResponseEntity.ok("");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }       
    }
    
    @PreAuthorize("hasRole('SUPER_ADMIN')")
    @PutMapping(value="/roles")
    public ResponseEntity updateRole(@RequestBody RoleDto roleDto) throws Exception{
        boolean isUpdated = roleService.update(roleDto);
        
        if(isUpdated){
            return ResponseEntity.ok("");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }       
    }
}
