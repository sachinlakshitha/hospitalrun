package ac.uk.bolton.hospitalrun.controller;

import ac.uk.bolton.hospitalrun.dto.AppoinmentDTO;
import ac.uk.bolton.hospitalrun.service.AppointmentService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:52
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api")
public class AppointmentController {

    @Autowired
    private AppointmentService appointmentService;

    @PostMapping(value = "/appointment/add")
    public ResponseEntity addAppointment(@RequestBody AppoinmentDTO dto) throws Exception {
        AppoinmentDTO appointment = appointmentService.save(dto);

        if(appointment == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @GetMapping(value="/appointment/{id}")
    public ResponseEntity getAppointment(@PathVariable Integer id) throws Exception{
        AppoinmentDTO appointment = appointmentService.findById(id);

        if(appointment == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(appointment);
        }
    }

    @GetMapping(value="/appointments")
    public ResponseEntity getAppointmentList() throws Exception{
        return ResponseEntity.ok(appointmentService.readAll());
    }

    @GetMapping(value="/patientId/{id}/appointment")
    public ResponseEntity getAppointmentsByPatientId(@PathVariable Integer id) throws Exception{
        List<AppoinmentDTO> appoinmentDTOList = appointmentService.readAppointmentsByPatientsId(id);

        if(appoinmentDTOList == null || appoinmentDTOList.isEmpty()){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(appoinmentDTOList);
        }
    }

    @GetMapping(value="/currentDay/appointment")
    public ResponseEntity getCurrentDayAppointmentList() throws Exception{
        List<AppoinmentDTO> appoinmentDTOList = appointmentService.getCurrentDateAppointment();

        if(appoinmentDTOList == null || appoinmentDTOList.isEmpty()){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(appoinmentDTOList);
        }
    }

    @GetMapping(value="/current/appointment/")
    public ResponseEntity getCurrentDateAppointments(@PathVariable Integer id) throws Exception{
        AppoinmentDTO appointment = appointmentService.findById(id);

        if(appointment == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(appointment);
        }
    }

    @DeleteMapping(value="/appointment/{id}")
    public ResponseEntity removeAppointment(@PathVariable Integer id) throws Exception{
        boolean isRemoved = appointmentService.deleteById(id);

        if(isRemoved){
            return ResponseEntity.ok("Appontment Removed Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value="/appointment/{id}")
    public ResponseEntity updateAppointment(@PathVariable Integer id,@RequestBody AppoinmentDTO dto) throws Exception{
        boolean isUpdated = appointmentService.update(id,dto);

        if(isUpdated){
            return ResponseEntity.ok("Appontment Details Modified Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
