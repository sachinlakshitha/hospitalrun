package ac.uk.bolton.hospitalrun.controller;

import ac.uk.bolton.hospitalrun.dto.PatientTypeDTO;
import ac.uk.bolton.hospitalrun.service.PatientTypeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 17:42
 */
@CrossOrigin(origins = "*", maxAge = 3600)
@RestController
@RequestMapping(value = "/api/patient")
public class PatientTypeController {

    @Autowired
    private PatientTypeService patientTypeService;

    @PostMapping(value = "/type")
    public ResponseEntity addPatientType(@RequestBody PatientTypeDTO dto) throws Exception {
        PatientTypeDTO patientType = patientTypeService.save(dto);

        if(patientType == null){
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }else{
            return ResponseEntity.status(HttpStatus.CREATED).build();
        }
    }

    @GetMapping(value="/type/{id}")
    public ResponseEntity getPatientType(@PathVariable Integer id) throws Exception{
        PatientTypeDTO patientType = patientTypeService.findById(id);

        if(patientType == null){
            return ResponseEntity.noContent().build();
        }else{
            return ResponseEntity.ok(patientType);
        }
    }

    @GetMapping(value="/types")
    public ResponseEntity getPatientTypeList() throws Exception{
        return ResponseEntity.ok(patientTypeService.readAll());
    }

    @DeleteMapping(value="/type/{id}")
    public ResponseEntity removePatientType(@PathVariable Integer id) throws Exception{
        boolean isRemoved = patientTypeService.deleteById(id);

        if(isRemoved){
            return ResponseEntity.ok("Patient Type Removed Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }

    @PutMapping(value="/type/{id}")
    public ResponseEntity updatePatientType(@PathVariable Integer id,@RequestBody PatientTypeDTO dto) throws Exception{
        boolean isUpdated = patientTypeService.update(id,dto);

        if(isUpdated){
            return ResponseEntity.ok("Patient Type Details Modified Sucessfully...");
        }else{
            return ResponseEntity.status(HttpStatus.INTERNAL_SERVER_ERROR).build();
        }
    }
}
