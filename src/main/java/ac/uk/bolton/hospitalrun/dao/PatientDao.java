package ac.uk.bolton.hospitalrun.dao;

import ac.uk.bolton.hospitalrun.model.Patient;

import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:07
 */
public interface PatientDao {
    public int save(Patient patient) throws Exception;
    public Patient findById(int id) throws Exception;
    public List<Patient> readAll() throws Exception;
    public int update(Patient patient) throws Exception;
    public int deleteById(int id) throws Exception;
}
