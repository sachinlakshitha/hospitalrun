/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dao.impl;

import ac.uk.bolton.hospitalrun.dao.PatientDao;
import ac.uk.bolton.hospitalrun.dao.RoleDao;
import ac.uk.bolton.hospitalrun.model.Patient;
import ac.uk.bolton.hospitalrun.model.PatientType;
import ac.uk.bolton.hospitalrun.model.Role;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author thisu96
 */
@Repository
public class PatientDaoImpl implements PatientDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(Patient patient) throws Exception {
        String sql = "INSERT INTO PATIENT (PATIENT_ID,PATIENT_TYPE_CODE,PREFIX,GIVEN_NAME,FAMILY_NAME," +
                "SUFFIX,GENDER,DATE_OF_BIRTH,P_LANGUAGE,OCCUPATION,PHONE_NUMBER,EMAIL_ADDRESS," +
                "ADDRESS) VALUES(:id,:typeCode,:prefix,:givenName," +
                ":familyName,:suffix,:gender,:dateOfBirth,:planguage,:occupation,:phoneNumber,:emailAddress,:address)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", patient.getPatientId());
        mapSqlParameterSource.addValue("typeCode", patient.getPatientTypeCode());
        mapSqlParameterSource.addValue("prefix", patient.getPrefix());
        mapSqlParameterSource.addValue("givenName", patient.getGivenName());
        mapSqlParameterSource.addValue("familyName", patient.getFamilyName());
        mapSqlParameterSource.addValue("suffix", patient.getSuffix());
        mapSqlParameterSource.addValue("gender", patient.getGender());
        mapSqlParameterSource.addValue("dateOfBirth", patient.getDateOfBirth());
        mapSqlParameterSource.addValue("planguage", patient.getLanguage());
        mapSqlParameterSource.addValue("occupation", patient.getOccupation());
        mapSqlParameterSource.addValue("phoneNumber", patient.getPhonenNumber());
        mapSqlParameterSource.addValue("emailAddress", patient.getEmailAddress());
        mapSqlParameterSource.addValue("address", patient.getAddress());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public Patient findById(int id) throws Exception {
        String sql = "SELECT * FROM PATIENT WHERE PATIENT_ID=?";
        List<Patient> patientList = null;
        try {
            patientList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Patient>>() {
                @Override
                public List<Patient> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Patient> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Patient patient = getPatient(resultSet);
                            temp.add(patient);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            },id);
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return !patientList.isEmpty() ? patientList.get(0) : null;
    }

    private Patient getPatient(ResultSet resultSet) throws SQLException,Exception {
        Patient patient = new Patient();

        patient.setPatientId(resultSet.getInt("PATIENT_ID"));
        patient.setPatientTypeCode(resultSet.getInt("PATIENT_TYPE_CODE"));
        patient.setPrefix(resultSet.getString("PREFIX"));
        patient.setGivenName(resultSet.getString("GIVEN_NAME"));
        patient.setFamilyName(resultSet.getString("FAMILY_NAME"));
        patient.setSuffix(resultSet.getString("SUFFIX"));
        patient.setGender(resultSet.getString("GENDER"));
        patient.setDateOfBirth(resultSet.getString("DATE_OF_BIRTH"));
        patient.setLanguage(resultSet.getString("P_LANGUAGE"));
        patient.setOccupation(resultSet.getString("OCCUPATION"));
        patient.setPhonenNumber(resultSet.getString("PHONE_NUMBER"));
        patient.setEmailAddress(resultSet.getString("EMAIL_ADDRESS"));
        patient.setAddress(resultSet.getString("ADDRESS"));

        return patient;
    }

    @Override
    public List<Patient> readAll() throws Exception {
        String sql = "SELECT * FROM PATIENT ORDER BY GIVEN_NAME ASC";
        List<Patient> patientList = null;
        try {
            patientList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Patient>>() {
                @Override
                public List<Patient> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Patient> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Patient patientType = getPatient(resultSet);
                            temp.add(patientType);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            });
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return patientList;
    }

    @Override
    public int update(Patient patient) throws Exception {
        String sql = "UPDATE PATIENT SET PATIENT_TYPE_CODE=:patientTypeCode,PREFIX=:prefix,GIVEN_NAME=:givenName,FAMILY_NAME=:familyName,SUFFIX=:suffix,GENDER=:gender," +
                "DATE_OF_BIRTH=:dateOfBirth,P_LANGUAGE=:planguage,OCCUPATION=:occupation,PHONE_NUMBER=:phoneNumber," +
                "EMAIL_ADDRESS=:emailAddress,ADDRESS=:address WHERE PATIENT_ID=:id";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", patient.getPatientId());
        mapSqlParameterSource.addValue("patientTypeCode", patient.getPatientTypeCode());
        mapSqlParameterSource.addValue("prefix", patient.getPrefix());
        mapSqlParameterSource.addValue("givenName", patient.getGivenName());
        mapSqlParameterSource.addValue("familyName", patient.getFamilyName());
        mapSqlParameterSource.addValue("suffix", patient.getSuffix());
        mapSqlParameterSource.addValue("gender", patient.getGender());
        mapSqlParameterSource.addValue("dateOfBirth", patient.getDateOfBirth());
        mapSqlParameterSource.addValue("planguage", patient.getLanguage());
        mapSqlParameterSource.addValue("occupation", patient.getOccupation());
        mapSqlParameterSource.addValue("phoneNumber", patient.getPhonenNumber());
        mapSqlParameterSource.addValue("emailAddress", patient.getEmailAddress());
        mapSqlParameterSource.addValue("address", patient.getAddress());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public int deleteById(int id) throws Exception {
        String sql = "DELETE FROM PATIENT WHERE PATIENT_ID=?";

        Object[] args = new Object[] {id};

        return jdbcTemplate.update(sql,args);
    }
}
