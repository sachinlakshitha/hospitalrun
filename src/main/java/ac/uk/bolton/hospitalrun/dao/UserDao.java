/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dao;

import ac.uk.bolton.hospitalrun.model.User;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface UserDao {
    public int save(User user) throws Exception; 
    public User findByUsername(String username);
    public User findById(String id) throws Exception;
    public List<User> readAll() throws Exception;
    public int update(User user) throws Exception;
    public int deleteById(String id) throws Exception;  
}
