/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dao;

import ac.uk.bolton.hospitalrun.model.Role;
import java.util.List;

/**
 *
 * @author Sachin
 */
public interface RoleDao {
    public int save(Role role) throws Exception;
    public Role findById(String id) throws Exception;
    public List<Role> readAll() throws Exception;
    public int update(Role role) throws Exception;
    public int deleteById(String id) throws Exception;
}
