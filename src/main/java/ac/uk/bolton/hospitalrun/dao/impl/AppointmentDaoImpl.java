package ac.uk.bolton.hospitalrun.dao.impl;

import ac.uk.bolton.hospitalrun.dao.AppointmentDao;
import ac.uk.bolton.hospitalrun.dto.AppoinmentDTO;
import ac.uk.bolton.hospitalrun.model.Appointment;
import ac.uk.bolton.hospitalrun.model.Patient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:52
 */
@Repository
public class AppointmentDaoImpl implements AppointmentDao {

    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(Appointment appoinment) throws Exception {
        String sql = "INSERT INTO APPOINTMENT (APPOINTMENT_ID,PATIENT_ID,START_DATE,END_DATE,LOCATION,APPOINTMENT_TYPE,REASON) VALUES(:id,:patientId,:startDate," +
                ":endDate,:location,:appointmentType,:reason)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", appoinment.getAppointmentId());
        mapSqlParameterSource.addValue("patientId", appoinment.getPatientId());
        mapSqlParameterSource.addValue("startDate", appoinment.getStartDate());
        mapSqlParameterSource.addValue("endDate", appoinment.getEndDate());
        mapSqlParameterSource.addValue("location", appoinment.getLocation());
        mapSqlParameterSource.addValue("appointmentType",appoinment.getAppointmentType());
        mapSqlParameterSource.addValue("reason", appoinment.getReason());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public Appointment findById(int id) throws Exception {
        String sql = "SELECT * FROM APPOINTMENT WHERE APPOINTMENT_ID=?";
        List<Appointment> appointmentList = null;
        try {
            appointmentList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Appointment>>() {
                @Override
                public List<Appointment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Appointment> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Appointment appointment = getAppointment(resultSet);
                            temp.add(appointment);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            },id);
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return !appointmentList.isEmpty() ? appointmentList.get(0) : null;
    }

    @Override
    public List<Appointment> readAll() throws Exception {
        String sql = "SELECT * FROM APPOINTMENT ORDER BY START_DATE ASC";
        List<Appointment> appointmentList = null;
        try {
            appointmentList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Appointment>>() {
                @Override
                public List<Appointment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Appointment> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Appointment appointment = getAppointment(resultSet);
                            temp.add(appointment);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            });
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return appointmentList;
    }

    @Override
    public int update(Appointment appoinment) throws Exception {
        String sql = "UPDATE APPOINTMENT SET PATIENT_ID=:patientId,START_DATE=:startDate,END_DATE=:endDate," +
                "LOCATION=:location,APPOINTMENT_TYPE=:appointmentType,REASON=:reason WHERE APPOINTMENT_ID=:id";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", appoinment.getAppointmentId());
        mapSqlParameterSource.addValue("patientId", appoinment.getPatientId());
        mapSqlParameterSource.addValue("startDate", appoinment.getStartDate());
        mapSqlParameterSource.addValue("endDate", appoinment.getEndDate());
        mapSqlParameterSource.addValue("location", appoinment.getLocation());
        mapSqlParameterSource.addValue("appointmentType", appoinment.getAppointmentType());
        mapSqlParameterSource.addValue("reason", appoinment.getReason());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public int deleteById(int id) throws Exception {
        String sql = "DELETE FROM APPOINTMENT WHERE APPOINTMENT_ID=?";

        Object[] args = new Object[] {id};

        return jdbcTemplate.update(sql,args);
    }

    @Override
    public List<Appointment> readAppointmentsByPatientsId(int patientId) {
        String sql = "SELECT * FROM APPOINTMENT WHERE PATIENT_ID=?";
        List<Appointment> appointmentList = null;
        try {
            appointmentList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Appointment>>() {
                @Override
                public List<Appointment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Appointment> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Appointment appointment = getAppointment(resultSet);
                            temp.add(appointment);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            },patientId);
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return !appointmentList.isEmpty() ? appointmentList : null;
    }

    @Override
    public List<Appointment> getCurrentDateAppointment() {
        String sql = "SELECT * FROM APPOINTMENT WHERE DATE(END_DATE) = CURDATE()";
        List<Appointment> appointmentList = null;
        try {
            appointmentList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Appointment>>() {
                @Override
                public List<Appointment> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Appointment> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            Appointment appointment = getAppointment(resultSet);
                            temp.add(appointment);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            });
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return appointmentList;
    }

    private Appointment getAppointment(ResultSet resultSet) throws SQLException,Exception {
        Appointment appointment = new Appointment();

        appointment.setAppointmentId(resultSet.getInt("APPOINTMENT_ID"));
        appointment.setPatientId(resultSet.getInt("PATIENT_ID"));
        appointment.setStartDate(resultSet.getTimestamp("START_DATE"));
        appointment.setEndDate(resultSet.getTimestamp("END_DATE"));
        appointment.setLocation(resultSet.getString("LOCATION"));
        appointment.setAppointmentType(resultSet.getString("APPOINTMENT_TYPE"));
        appointment.setReason(resultSet.getString("REASON"));

        return appointment;
    }
}
