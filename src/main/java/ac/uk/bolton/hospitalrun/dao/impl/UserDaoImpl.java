/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dao.impl;

import ac.uk.bolton.hospitalrun.dao.UserDao;
import ac.uk.bolton.hospitalrun.model.User;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sachin
 */
@Repository
public class UserDaoImpl implements UserDao{
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
	this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public int save(User user) throws Exception {
        String sql = "INSERT INTO USER (USER_ID,USERNAME,PASSWORD,ROLE_ID) VALUES(:id,:username,:password,:roleid)";
        
        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();
        
        mapSqlParameterSource.addValue("id", user.getUserId());
        mapSqlParameterSource.addValue("username", user.getUsername());
        mapSqlParameterSource.addValue("password", user.getPassword());
        mapSqlParameterSource.addValue("roleid", user.getRoleId());
                
        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }
    
    @Override
    public User findByUsername(String username) {
        String sql = "SELECT * FROM USER WHERE USERNAME=?";
        
        List<User> userList = null;
	try {
            userList = jdbcTemplate.query(sql,new ResultSetExtractor<List<User>>() {

		@Override
                public List<User> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<User> userTemp = new ArrayList<User>();
                    
                    while (resultSet.next()) {
                        try {
                            User user = getFullUser(resultSet);
                            userTemp.add(user);
                        } catch (Exception ex) {
                            
                        }
                    }
                    
                    return userTemp;
		}
            },username);

	} catch (DataAccessException dataAccessException) {
            throw dataAccessException;

	} catch (Exception exception) {

	} 
        
        
	return !userList.isEmpty() ? userList.get(0) : null;
    }
    
    private User getFullUser(ResultSet resultSet) throws SQLException,Exception {
        User user = new User();
        
        user.setUserId(resultSet.getString("USER_ID"));
        user.setUsername(resultSet.getString("USERNAME"));
        user.setPassword(resultSet.getString("PASSWORD"));
        user.setRoleId(resultSet.getString("ROLE_ID"));
                                    
        return user;
        
    }
    
    private User getUser(ResultSet resultSet) throws SQLException,Exception {
        User user = new User();
        
        user.setUserId(resultSet.getString("USER_ID"));
        user.setUsername(resultSet.getString("USERNAME"));
        user.setRoleId(resultSet.getString("ROLE_ID"));
                                    
        return user;
        
    }

    @Override
    public List<User> readAll() throws Exception {
        String sql = "SELECT * FROM USER ORDER BY USERNAME ASC";
        
        List<User> userList = null;
	try {
            userList = jdbcTemplate.query(sql,new ResultSetExtractor<List<User>>() {

		@Override
                public List<User> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<User> userTemp = new ArrayList<User>();
                    
                    while (resultSet.next()) {
                        try {
                            User user = getUser(resultSet);
                            userTemp.add(user);
                        } catch (Exception ex) {
                            
                        }
                    }
                    
                    return userTemp;
		}
            });

	} catch (DataAccessException dataAccessException) {
            throw dataAccessException;

	} catch (Exception exception) {

	} 
        
        
	return userList;
    }    

    @Override
    public User findById(String id) throws Exception {
        String sql = "SELECT * FROM USER WHERE USER_ID=?";
        
        List<User> userList = null;
	try {
            userList = jdbcTemplate.query(sql,new ResultSetExtractor<List<User>>() {

		@Override
                public List<User> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<User> userTemp = new ArrayList<User>();
                    
                    while (resultSet.next()) {
                        try {
                            User user = getFullUser(resultSet);
                            userTemp.add(user);
                        } catch (Exception ex) {
                            
                        }
                    }
                    
                    return userTemp;
		}
            },id);

	} catch (DataAccessException dataAccessException) {
            throw dataAccessException;

	} catch (Exception exception) {

	} 
        
        
	return !userList.isEmpty() ? userList.get(0) : null;
    }

    @Override
    public int deleteById(String id) throws Exception {
        String sql = "DELETE FROM USER WHERE USER_ID=?";
        
        Object[] args = new Object[] {id};
       
        return jdbcTemplate.update(sql,args);
    }

    @Override
    public int update(User user) throws Exception {
        String sql = "";
        
        if(user.getPassword() == null){
            sql = "UPDATE USER SET USERNAME=:username,ROLE_ID=:roleid WHERE USER_ID=:id";
        }else{
            sql = "UPDATE USER SET USERNAME=:username,PASSWORD=:password,ROLE_ID=:roleid WHERE USER_ID=:id";
        }
                
        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();
                    
        mapSqlParameterSource.addValue("id", user.getUserId());
        mapSqlParameterSource.addValue("username", user.getUsername());
        mapSqlParameterSource.addValue("password", user.getPassword());
        mapSqlParameterSource.addValue("roleid", user.getRoleId());
                
        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }
    
}
