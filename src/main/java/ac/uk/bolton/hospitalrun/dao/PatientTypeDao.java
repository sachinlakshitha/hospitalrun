package ac.uk.bolton.hospitalrun.dao;

import ac.uk.bolton.hospitalrun.model.PatientType;
import ac.uk.bolton.hospitalrun.model.Role;

import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:35
 */
public interface PatientTypeDao {
    public int save(PatientType patientType) throws Exception;
    public PatientType findById(int id) throws Exception;
    public List<PatientType> readAll() throws Exception;
    public int update(PatientType patientType) throws Exception;
    public int deleteById(int id) throws Exception;
}
