package ac.uk.bolton.hospitalrun.dao;

import ac.uk.bolton.hospitalrun.dto.AppoinmentDTO;
import ac.uk.bolton.hospitalrun.model.Appointment;
import ac.uk.bolton.hospitalrun.model.Patient;

import java.util.List;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:52
 */
public interface AppointmentDao {
    public int save(Appointment appoinment) throws Exception;
    public Appointment findById(int id) throws Exception;
    public List<Appointment> readAll() throws Exception;
    public int update(Appointment appoinment) throws Exception;
    public int deleteById(int id) throws Exception;
    public List<Appointment> readAppointmentsByPatientsId(int patientId);
    public List<Appointment> getCurrentDateAppointment();
}
