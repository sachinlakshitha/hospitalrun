package ac.uk.bolton.hospitalrun.dao.impl;

import ac.uk.bolton.hospitalrun.dao.PatientTypeDao;
import ac.uk.bolton.hospitalrun.model.PatientType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

import javax.sql.DataSource;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * @author thisu96
 * @Date 01/07/2020
 * @Time 09:36
 */
@Repository
public class PatientTypeDaoImpl implements PatientTypeDao {
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
        this.jdbcTemplate = new JdbcTemplate(dataSource);
    }

    @Override
    public int save(PatientType patientType) throws Exception {
        String sql = "INSERT INTO PATIENT_TYPE (PATIENT_TYPE_CODE,PATIENT_TYPE_DESC) VALUES(:id,:patientTypeDesc)";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", patientType.getPatientTypeCode());
        mapSqlParameterSource.addValue("patientTypeDesc", patientType.getPatientTypeDesc());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public PatientType findById(int id) throws Exception {
        String sql = "SELECT * FROM PATIENT_TYPE WHERE PATIENT_TYPE_CODE=?";
        List<PatientType> patientTypeList = null;
        try {
            patientTypeList = jdbcTemplate.query(sql,new ResultSetExtractor<List<PatientType>>() {
                @Override
                public List<PatientType> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<PatientType> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            PatientType patientType = getPatientType(resultSet);
                            temp.add(patientType);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            },id);
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return !patientTypeList.isEmpty() ? patientTypeList.get(0) : null;
    }

    private PatientType getPatientType(ResultSet resultSet) throws SQLException,Exception {
        PatientType patientType = new PatientType();

        patientType.setPatientTypeCode(resultSet.getInt("PATIENT_TYPE_CODE"));
        patientType.setPatientTypeDesc(resultSet.getString("PATIENT_TYPE_DESC"));

        return patientType;
    }

    @Override
    public List<PatientType> readAll() throws Exception {
        String sql = "SELECT * FROM PATIENT_TYPE ORDER BY PATIENT_TYPE_DESC ASC";
        List<PatientType> patientTypeList = null;
        try {
            patientTypeList = jdbcTemplate.query(sql,new ResultSetExtractor<List<PatientType>>() {
                @Override
                public List<PatientType> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<PatientType> temp = new ArrayList<>();
                    while (resultSet.next()) {
                        try {
                            PatientType patientType = getPatientType(resultSet);
                            temp.add(patientType);
                        } catch (Exception ex) {

                        }
                    }
                    return temp;
                }
            });
        } catch (DataAccessException dataAccessException) {
            throw dataAccessException;
        } catch (Exception exception) {

        }
        return patientTypeList;
    }

    @Override
    public int update(PatientType patientType) throws Exception {
        String sql = "UPDATE PATIENT_TYPE SET PATIENT_TYPE_DESC=:patientType WHERE PATIENT_TYPE_CODE=:id";

        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();

        mapSqlParameterSource.addValue("id", patientType.getPatientTypeCode());
        mapSqlParameterSource.addValue("patientType", patientType.getPatientTypeDesc());

        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public int deleteById(int id) throws Exception {
        String sql = "DELETE FROM PATIENT_TYPE WHERE PATIENT_TYPE_CODE=?";

        Object[] args = new Object[] {id};

        return jdbcTemplate.update(sql,args);
    }
}
