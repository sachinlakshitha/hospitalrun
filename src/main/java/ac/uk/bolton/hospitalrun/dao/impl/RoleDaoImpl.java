/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dao.impl;

import ac.uk.bolton.hospitalrun.dao.RoleDao;
import ac.uk.bolton.hospitalrun.model.Role;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import javax.sql.DataSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.namedparam.MapSqlParameterSource;
import org.springframework.jdbc.core.namedparam.NamedParameterJdbcTemplate;
import org.springframework.stereotype.Repository;

/**
 *
 * @author Sachin
 */
@Repository
public class RoleDaoImpl implements RoleDao{
    private JdbcTemplate jdbcTemplate;

    @Autowired
    public void setDatasource(DataSource dataSource) {
	this.jdbcTemplate = new JdbcTemplate(dataSource);
    }
    
    @Override
    public int save(Role role) throws Exception {
        String sql = "INSERT INTO ROLE (ROLE_ID,ROLE_NAME) VALUES(:id,:rolename)";
        
        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();
        
        mapSqlParameterSource.addValue("id", role.getRoleId());
        mapSqlParameterSource.addValue("rolename", role.getRoleName());
                
        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }
    
    @Override
    public Role findById(String id) throws Exception {
        String sql = "SELECT * FROM ROLE WHERE ROLE_ID=?";
        
        List<Role> roleList = null;
	try {
            roleList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Role>>() {

		@Override
                public List<Role> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Role> roleTemp = new ArrayList<>();
                    
                    while (resultSet.next()) {
                        try {
                            Role role = getRole(resultSet);
                            roleTemp.add(role);
                        } catch (Exception ex) {
                            
                        }
                    }
                    
                    return roleTemp;
		}
            },id);

	} catch (DataAccessException dataAccessException) {
            throw dataAccessException;

	} catch (Exception exception) {

	} 
        
        
	return !roleList.isEmpty() ? roleList.get(0) : null;
    }
    
    private Role getRole(ResultSet resultSet) throws SQLException,Exception {
        Role role = new Role();
        
        role.setRoleId(resultSet.getString("ROLE_ID"));
        role.setRoleName(resultSet.getString("ROLE_NAME"));
        
        return role;
    }

    @Override
    public List<Role> readAll() throws Exception {
        String sql = "SELECT * FROM ROLE ORDER BY ROLE_NAME ASC";
        
        List<Role> roleList = null;
	try {
            roleList = jdbcTemplate.query(sql,new ResultSetExtractor<List<Role>>() {

		@Override
                public List<Role> extractData(ResultSet resultSet) throws SQLException, DataAccessException {
                    List<Role> roleTemp = new ArrayList<>();
                    
                    while (resultSet.next()) {
                        try {
                            Role role = getRole(resultSet);
                            roleTemp.add(role);
                        } catch (Exception ex) {
                            
                        }
                    }
                    
                    return roleTemp;
		}
            });

	} catch (DataAccessException dataAccessException) {
            throw dataAccessException;

	} catch (Exception exception) {

	} 
        
        
	return roleList;
    }

    @Override
    public int update(Role role) throws Exception {
        String sql = "UPDATE ROLE SET ROLE_NAME=:rolename WHERE ROLE_ID=:id";
        
        NamedParameterJdbcTemplate namedParameterJdbcTemplate=new NamedParameterJdbcTemplate(jdbcTemplate.getDataSource());
        MapSqlParameterSource mapSqlParameterSource=new MapSqlParameterSource();
                    
        mapSqlParameterSource.addValue("id", role.getRoleId());
        mapSqlParameterSource.addValue("rolename", role.getRoleName());
                
        return namedParameterJdbcTemplate.update(sql, mapSqlParameterSource);
    }

    @Override
    public int deleteById(String id) throws Exception {
        String sql = "DELETE FROM ROLE WHERE ROLE_ID=?";
        
        Object[] args = new Object[] {id};
       
        return jdbcTemplate.update(sql,args);
    }

}
