/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 *
 * @author thisu96
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class PatientDTO {
    private int patientId;
    private int patientTypeCode;
    private String prefix;
    private String givenName;
    private String familyName;
    private String suffix;
    private String gender;
    private String dateOfBirth;
    private String language;
    private String occupation;
    private String phonenNumber;
    private String emailAddress;
    private String address;
}
