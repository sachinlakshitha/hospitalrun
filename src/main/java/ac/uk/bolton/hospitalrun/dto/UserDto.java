/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ac.uk.bolton.hospitalrun.dto;

/**
 *
 * @author Sachin
 */
public class UserDto {
    private String userId;
    private String username;
    private String role;

    public String getUserId() {
        return userId;
    }

    public void setUserId(String userId) {
        this.userId = userId;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
  
    public String getRole() {
        return role;
    }

    public void setRole(String role) {
        this.role = role;
    }
}
