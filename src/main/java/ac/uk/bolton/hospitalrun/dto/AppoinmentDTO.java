package ac.uk.bolton.hospitalrun.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

import java.util.Date;

/**
 * @author thisu96
 * @Date 05/07/2020
 * @Time 16:53
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
public class AppoinmentDTO {
    private Integer appointmentId;
    private Integer patientId;
    private String startDate;
    private String endDate;
    private String location;
    private String appointmentType;
    private String reason;
    private PatientDTO patient;
}
